const express = require("express");
const app = express();
// const request = require("request");

const bodyParser = require("body-parser");
app.use(bodyParser.json());

const router = require("./routes/api.route.js");
app.use('/', router);



const PORT = process.env.PORT || 5000 ;
app.listen( PORT, ()=>{
    console.log(`START with ${PORT}`);
})
