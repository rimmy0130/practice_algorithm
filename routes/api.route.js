const router = require("express").Router();


//20200731 - 20200308 13:00
//문제 1
router.post('/exam001',(req,res)=>{
    /* 
       알고리즘 로직 작성 구간
    */
    var body = req.body ;

    body.forEach(item => {
    //    console.log(parseInt(item.input))

        // if(!isNaN(item.input)){
        //     item.output = item.input ;
        // }else{
            item.output  = item.input.replace(/[^0-9]/gi, "");
            if(item.output  === ""){
                item.output  = "NaN";
            }
        // }
    });
    res.json(body);
})


//문제 2
router.post('/exam002',(req,res)=>{
    var body = req.body;

    body.forEach(item => {
        var point = item.input[0] / (item.input[2] - item.input[1]) ;

        if(point > 1){
            item.output = Math.floor(point+1);

        }else{
            item.output = -1 ;
        }
    })

    res.json(body);
})


//문제 3
router.post('/exam003',(req,res)=>{
    var body = req.body;

    function getCountryANumber( num ){
        var countryA = ['6','2','4'] ;
        var answer = "";
        var num = num;


        while(num >= 1){
            var remainder =  num % 3 ;
            num = Math.floor(num / 3);
            // num = Math.floor(num);
            
            if(remainder === 0){
                num -= 1 ;
            }
            // console.log(countryA[remainder])
            answer = countryA[remainder] + answer ;
        }
        return answer ;
    }

    body.forEach(item => {
        var answer = getCountryANumber(item.input);
        item.output = answer;
    })

    res.json(body);
});




//하노이의 탑
router.post('/exam004',(req,res)=>{
    var body = req.body;
    body.output = [];

    hanoi(body.input,"A","B","C");
    
    function hanoi(n, from, temp, to){
        if(n === 1){
            body.output.push([from, to]);  //
            // console.log(`${n}을 ${from}에서 ${to}로 이동`);
        }else{
            hanoi(n-1, from, to, temp);   //시작에서 중간지점으로 이동
            body.output.push([from,to]);
            // console.log(`${n}을 ${from}에서 ${to}로 이동`);
            hanoi(n-1, temp, from, to);  //중간지점에서 끝으로
        }
    }
    res.send(body);

});  //exam004 END


//선택정렬
router.post('/exam005',(req,res)=>{
    var body = req.body.input;
    var swap ;

    for(var i=0; i<body.length; i++){
        var minIdx = i;
        for(var j=i+1; j<body.length; j++){
            if(body[minIdx] > body[j]){
                minIdx = j;
            }
        }
        swap = body[i];
        body[i] = body[minIdx];
        body[minIdx] = swap;
        // console.log(body);
    }
    // req.body.output = body;
    // res.send(req.body.output)
    res.send(body)

});

//삽입정렬
router.post('/exam006',(req,res)=>{
    var body = req.body.input;
    
    for(var i=1; i<body.length; i++){
        var key = body[i] ;
        for(var j=i-1; j>=0; j--){
            if(body[j] > key){
                body[j+1] = body[j] ;
                body[j] = key;
            }else{
                break;
            }
        }
        console.log(body);
    }

    res.send(body)
});


//버블정렬
router.post('/exam007',(req,res)=>{
    var body = req.body.input;
    var swap;
    for(var i=0; i<body.length; i++){  
        for(var j=0; j<body.length-(i+1);j++){
            if(body[j] > body[j+1]){
                swap = body[j+1];
                body[j+1] = body[j];
                body[j] = swap;
            }

        }
    }
    res.send(body)
});


//퀵정렬 Not In Place
router.post('/exam008-1',(req,res)=>{
    var body = req.body.input;
    var answer = quickSort(body);
    
    function quickSort(arr){
        var left = [];
        var right = [];
        var arrLen = arr.length;
        var pivot ;
        if(arrLen > 1){
            pivot = arr[0];
            for(var idx=1; idx<arrLen; idx++){
                if(arr[idx] > pivot) right.push(arr[idx])
                else left.push(arr[idx])
            }

            var leftArr = quickSort(left); 
            var rightArr = quickSort(right);

           return leftArr.concat(pivot, rightArr);
        }else{
            return arr;
        }
    } // function END
    res.send(answer);
});

//퀵정렬 In Place
router.post("/exam008-2", (req,res)=>{
    var body = req.body.input;
    var answer = quickSort(body);

    function quickSort(arr){
        var arrLen = arr.length;
        var left = [];
        var right = [];
        var middle = [];
        
        if(arrLen > 2){
            var pivot = arr[ Math.floor(arrLen/2) ];
            for(var idx=0; idx<arrLen; idx++){
                if(arr[idx] > pivot) right.push(arr[idx])
                else if(arr[idx] < pivot) left.push(arr[idx])
                else middle.push(arr[idx])
            }
            
            var leftArr = quickSort(left); 
            var rightArr = quickSort(right);

            return leftArr.concat(middle, rightArr);
        }
        else{
            return arr;
        }
    }
    res.send(answer);
});

router.post("/exam009", (req,res)=>{
    var inputValue = req.body.input;
    var answer = "";

    var arr = [];
    for(var arrIdx=1; arrIdx<=100000; arrIdx++){
        arr.push(arrIdx);
    };

    var count = 0;
    var leftIdx = 0;
    var rightIdx = arr.length-1;
    if(arr[rightIdx] < inputValue || arr[leftIdx] > inputValue){
        answer = `탐색 횟수 : ${count}회, ${inputValue}을 찾지 못했습니다.`;
        res.send(answer);
    }

    // for(var i=0; ;i++){
    while(leftIdx <= rightIdx){
        var midIdx = Math.floor((leftIdx + rightIdx) / 2);
        var pivot = arr[ midIdx ];   //중앙값 찾기
        count += 1 ;

        if(pivot > inputValue){
            rightIdx = midIdx;
        }else if(pivot < inputValue){
            leftIdx = midIdx + 1;
        }else if(pivot === inputValue){
            answer = `탐색 횟수 : ${count}회, [${midIdx}] 위치에서 ${inputValue}을 찾았습니다.`;
            break;
        }
    }
    res.send(answer);
})

router.post("/exam010", (req,res)=>{
    var m = req.body.m;
    var treeArr = req.body.tree;
    var answer;

    treeArr.sort();
    var leftIdx = 0;
    var rightIdx = treeArr.length-1;
    while(leftIdx < rightIdx){
        var midIdx = Math.floor((leftIdx + rightIdx) / 2);
        var pivot = treeArr[ midIdx ];   //중앙값 찾기
        var sumTree = 0;
        for(var idx=0; idx<treeArr.length; idx++){
            if(treeArr[idx] > pivot){
                sumTree += treeArr[idx]-pivot ;
            }
        }
        if(sumTree === m){
            answer = pivot;
            break;
        }else if(sumTree > m){
            leftIdx = midIdx + 1;
        }else if(sumTree < m){
            rightIdx = midIdx;
        }
    }
    req.body.output = answer;
    res.send(req.body);
})

module.exports = router; 